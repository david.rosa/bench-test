data "google_dns_managed_zone" "bench_test" {
  name     = "bench-test"
}

resource "google_dns_record_set" "bench-test" {
  name = data.google_dns_managed_zone.bench_test.dns_name
  type = "A"
  ttl  = 300

  managed_zone = data.google_dns_managed_zone.bench_test.name

  rrdatas = [var.service_ip]
}