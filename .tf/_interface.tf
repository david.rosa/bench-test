terraform {
  backend "gcs" {
    bucket  = "tf-state-bench-test"
    prefix  = "terraform/state"
  }
}

provider google {
    project = "david-resources"
}

variable "service_ip" {
  type = string
}