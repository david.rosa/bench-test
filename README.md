# Bench Test

This is a test for [bench.co](https://bench.co)

It can be accessed in the domain [bench-test.davidrosa.me](http://bench-test.davidrosa.me)

## About project

This project uses mainly the following technologies
- Python
- Terraform
- Kubernetes
- Gitlab CI

### Design choices
To make the project simpler, some design choices were made, this led to limitations. Some that are briefly discussed below

This project also assumes that some resources (such as the K8S cluster and the DNS zone are created beforehand)

Tests were neglected for this application (I got the impression that the code development itself wasn't the focus)

### Some known limitations

#### Python
It uses gunicorn as WSGI. Gunicorn is a server suitable for production, but it shouldn't be exposed directly to the internet (as this project does) due to slow client attacks which this server is vulnerable. The correct way would be to setup a reverse proxy with something like a nginx.

This app also is not using HTTPS.

#### Terraform
Currently there is no local testing for the terraform in this project (because having so would require setting up another domain for testing)

#### Kubernetes
The app uses the default namespace, which is not optimal for production environments, due to security concerns.

There are no error metrics configured.

#### GitlabCI
In a real-world environment, more validation would be needed to update the registry and the k8s deployment

This project has no vulnerability scanning nor static or dynamic code analysis

The CI is deploying any tag, in a real-world environment it should only deploy the protected tags