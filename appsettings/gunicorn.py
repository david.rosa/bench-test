import multiprocessing

# depending on your environment you might not wan't to bind to any IP address
bind = "0.0.0.0:5000"
workers = multiprocessing.cpu_count() * 2 + 1
threads = workers * 4