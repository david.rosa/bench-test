#!/usr/bin/env python3

# I didn't spent too much effort in the code itself, as I think it's not the focus of the challenge
# For a read app I wouldn't put everything in the main and I'd properly map the routes, have models, et cetera
# (and I'd probably wouldn't use Flask also 💁🏼‍♂️)

from flask import Flask
from flask import jsonify
from time import time

START_TIME = time()

def isready():
    # this could eventually be cached
    return time() - START_TIME >= 10

app = Flask(__name__)

@app.route('/')
def root():
    return 'Hello World!'

@app.route('/status/alive')
def alive():
    return '', 200

@app.route('/status/ready')
def ready():
    return (jsonify(ready=True), 200) if isready() else (jsonify(ready=False), 500)

# for local testing (no docker) only, docker will use gunicorn
if __name__ == '__main__':
    app.run(host= '0.0.0.0', port=2323)