#!/bin/bash

# using latest here just for convinience, in the CI it uses the tag as version
POD_IMAGE="bench-test:latest"

docker build -t $POD_IMAGE .

envsubst < .k8s/spec.yml | kubectl delete -f -
envsubst < .k8s/spec.yml | kubectl apply -f -