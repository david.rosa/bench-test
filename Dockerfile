FROM python:3.8.2-alpine

# Set the working directory to /app
WORKDIR /app

# first install just the requirements so Docker can cache it
COPY ./requirements.txt /app/
RUN python -m pip install -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app

EXPOSE 5000

# to disable eventual  `if __debug__:` code
ENV PYTHONOPTIMIZE 2

CMD gunicorn -c appsettings/gunicorn.py main:app
